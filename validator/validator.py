#!/usr/bin/env python3

#
#                Validator for maximum disjoint dominating sets problem 
#                            -------------------
#   begin                : Thu 31 Mar 2022
#   copyright            : (C) 2022 by Roberto Maria Rosati
#   email                : robertomaria.rosati@uniud.it
#
#
#                                                                         
#   This program is free software; you can redistribute it and/or modify  
#   it under the terms of the GNU General Public License as published by  
#   the Free Software Foundation; either version 2 of the License, or     
#   (at your option) any later version.                                   
#                                                                         
#

import numpy as np
import pandas as pd
import argparse

def validate_instance(in_file):
  with_errors = False
  with open(in_file, "r") as file:
    fileinstance = file.read()
  instance = fileinstance.split("\n") #I read the instance as a list with each element of the list one line of the original file
  first_line = instance[0].strip().split(" ")
  if(len(first_line) != 2):
    with_errors = True
    print(f"Instance file: ERROR: line 0 of the instance expects two arguments: vertices and edges. {len(first_line)} are given.")
  if(len(first_line) >= 1):
    nodes = int(first_line[0])
  i=1
  lines = min(nodes + 1, len(instance))
  if(lines < nodes + 1):
    with_errors = True
    print(f"Instance file: ERROR: Adjaciency matrix must have {nodes} rows, found instead: {lines-1}.")
  while(i < lines):
    this_line = instance[i].strip().split(" ")
    if(len(this_line) != nodes):
      with_errors = True
      print(f"Instance file: ERROR: at line {i} of the instance {nodes} elements expected, found: {len(this_line)}.")
    j = 0
    while (j<len(this_line)):
      if (this_line[j] != "0" and this_line[j] != "1"):
        print(f"Instance file: ERROR: at line {i} found elements with values different from 0 and 1, found: {this_line[j]}")
        with_errors = True
      j = j+1
    i = i+1
  while(i < len(instance)):
    this_line = instance[i].strip()
    if(this_line != ""):
      print(f"Instance file: ERROR: Nothing should be written after adjaciency matrix. Found instead something at line {i}")
      with_errors = True
    i = i+1
  
  #I check adjacency matrix
  instance = fileinstance.strip().split("\n")
  for i in range(0,len(instance)):
    instance[i] = instance[i].strip().split(" ")
  bin_adj = []
  for i in range(1, nodes+1):
    bin_adj.append([])
    for j in range (0,nodes):
      bin_adj[-1].append(int(instance[i][j]))
  
  for i in range(0, nodes):
    for j in range(0, nodes):
      if (bin_adj[i][j]!=bin_adj[j][i]):
        print(f"Instance file: ERROR in the adjacency matrix: adj[{i}][{j}] != adj[{j}][{i}] ({bin_adj[i][j]} != {bin_adj[j][i]})")
        with_errors = True
  return(not with_errors)

def getinputdata(in_file):
  with open(in_file, "r") as file:
    instance = file.read()
  instance = instance.strip().split("\n")
  for i in range(0,len(instance)):
    instance[i] = instance[i].strip().split(" ")
  nodes = int(instance[0][0])
  adj = []
  for i in range(1, nodes+1):
    adj.append([])
    for j in range(0, nodes):
      if(int(instance[i][j]) == 1):
        adj[-1].append(j)

  return nodes,adj

def validate_solution(in_file, sol_file):
  if(not validate_instance(in_file)):
    print(f"ERRORs found in instance file. I will not continue validating solution. Exiting...")
    return(False)
  nodes, adj = getinputdata(in_file) #I read the number of nodes in the input and the adjacency matrix

  # I import the solution
  sets = []
  with open(sol_file, "r") as file:
    solution = file.read()
  solution = solution.strip().split("\n") #I read the instance as a list with each element of the list one line of the original file
  for line in solution:
    this_line = line.strip().split(" ")
    for i in range (0,len(this_line)):
      this_line[i] = int(this_line[i])
    sets.append(this_line)

  objective_value = len(sets)
  #now that I have the sets in a list of ints, I have to check that: 
  #1. I check that all sets are ordered 
  #         (this is not strictly necessary for feasibility, but it's a requirement we want to be respected)
  #2. sets in sols don't comprise nodes that are not in sol
  #3. all the sets in sols are disjoint
  #4. the sets in sol are dominating sets for the instance
  if (objective_value == 0):
    return 0
  
  #1 and 2
  if(sets[0][0] > nodes):
     print(f"Solution ERROR: line 1, vertex 1 has value {sets[0][0]}, but number of nodes is: {nodes}")
     objective_value = -1
  for i in range(0,len(sets)):
    for j in range(1,len(sets[i])):
      if(sets[i][j] > nodes):
        print(f"Solution ERROR: line {i+1}, vertex {j+1} has value {sets[i][j]}, but number of nodes is: {nodes}")
        objective_value = -1
      if(sets[i][j-1] > sets[i][j]):
        print(f"Solution ERROR: set at line {i+1} not sorted, vertex at position {j} is higher then vertex at position position {j+1}! Values: {sets[i][j-1]} > {sets[i][j]}")
        objective_value = -1
  
  #3
  for i in range(0,len(sets)-1):
    for j in range(i+1,len(sets)):
      intersect = [value for value in sets[i] if value in sets[j]]
      if(len(intersect) > 0):
        print(f"Solution ERROR: sets at lines {i+1} and {j+1} have elements in common: {intersect}! This means that the sets in the solution are not disjoint!")
        objective_value = -1

  #4
  for i in range(0,len(sets)): 
    covered = [False]*nodes
    for j in range(0,len(sets[i])):
      n = sets[i][j]
      covered[n] = True
      for an in adj[n]:
        covered[an] = True
    count_cover = covered.count(True)
    if(count_cover != nodes):
      outside = []
      for k in range(0,len(covered)):
        if covered[k] == False:
          outside.append(k)
      print(f"Solution ERROR: set at line {i+1} is not a dominating set, vertices {nodes-count_cover} are neither dominating or dominated: {outside}!")
      objective_value = -1

  return objective_value

def main(args):
  if(args.solution == None):
    validate_instance(args.input)
  else:
    obj = validate_solution(args.input,args.solution)
    if(obj != -1):
      print(obj)
    else:
      print("ERRORS in Solution were found")

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = "Returns the cost of the solution, or an error if the solution is not valid")
  parser.add_argument("--input", "-i", required = True, type = str, help = "instance file")
  parser.add_argument("--solution", "-s", required = False, type = str, help = "solution file")
  args = parser.parse_args()
  main(args)
   