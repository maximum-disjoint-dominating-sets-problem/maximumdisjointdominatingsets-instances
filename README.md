# Maximum Disjoint Dominating Sets

Instances and an instance/solution validator for the Maximum Disjoint Dominating Sets

## Instances

All the instances are under the "instances" folder.

  - instances/toy: a couple of small toy instances
  - instances/random_graphs: 
    - instances/random_graphs/MIC2022: see [1] for details and results on this dataset
    - instances/random_graphs/COR-hammersley_training: see [2] for details about the generation procedure, this dataset was used in the training phase of [2] 
    - instances/random_graphs/COR-hammersley_validation: see [2] for details about the generation procedure and results on this validation dataset
    - instances/random_graphs/larger: larger random graphs not used so far
  - instances/random_geometric_graphs: 
    - instances/random_geometric_graphs/small: small random geometric graphs, they can be used for testing purposes 
    - instances/random_geometric_graphs/validation: see [1] for details and results on this dataset
  - instances/barabasi-albert/validation: Barabasi-Albert networks, see [2] for details and results on this dataset
  - instances/watts-strogatz/validation: Watts-Strogatz networks, see [2] for details and results on this dataset

Folder "validator" contains the instance and solution validator

Folder "solutions" contains sample solutions for two toy instances


## Validator
To validate an instance:
/path/to/validator -i path/to/instance/file

To validate a solution:
/path/to/validator -i path/to/instance/file -s path/to/solution/file

Solution is expected to be a plain text file containing a dominating set (indexes of the vertices) in each row. See folder example for two examples of solutions. When validating a solution, output will be the objective value of the solution (number of dominating sets), if the validation is successful. If the solution is not a feasible solution for the instance, the validator will return a detailed error message.

## License

This instances are free software, you can redistribute it under the terms of the GNU General Public License as published by the Free Software Foundation.

## References/How to cite

If you use these instances, please cite the works:

[1] Rosati RM, Bouamama S, Blum C (2023) Construct, merge, solve and
adapt applied to the maximum disjoint dominating sets problem. In:
Metaheuristics, pp 306–321

[2] Rosati RM, Bouamama S, Blum C (2024) Multi-constructor CMSA for the
maximum disjoint dominating sets problem. Computers & Operations
Research 161:106450

Respective .bibtex entries:
```
@inproceedings{RoBoBl2022MIC,
author = {Rosati, Roberto Maria and Bouamama, Salim and Blum, Christian},
editor = {Di Gaspero, Luca and Festa, Paola and Nakib, Amir and Pavone, Mario},
title = {Construct, Merge, Solve and Adapt Applied to the Maximum Disjoint Dominating Sets Problem},
booktitle = {Metaheuristics},
year = {2023},
publisher = {Springer International Publishing},
pages = {306--321},
isbn = {978-3-031-26504-4}}
```

```
@article{RoBoBl2024multiconstrcmsa,
title = {Multi-constructor {CMSA} for the maximum disjoint dominating sets problem},
journal = {Computers \& Operations Research},
volume = {161},
pages = {106450},
year = {2024},
doi = {https://doi.org/10.1016/j.cor.2023.106450},
url = {https://www.sciencedirect.com/science/article/pii/S0305054823003143},
author = {Roberto Maria Rosati and Salim Bouamama and Christian Blum}}
```






